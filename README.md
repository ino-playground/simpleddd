# DDD で シンプルな API サーバを実装してみる

## 参考

- [【Go】DDD + レイヤードアーキテクチャで REST API を実装する | みんたく](https://mintaku-blog.net/go-ddd/)
- [リポジトリと DAO の棲み分け方 – 福地春喜のブログ](https://blog.fukuchiharuki.me/entry/use-repository-and-dao-according-to-the-purpose)
- [今すぐ「レイヤードアーキテクチャ+DDD」を理解しよう。（golang） - Qiita](https://qiita.com/tono-maron/items/345c433b86f74d314c8d)
- [Go の package 構成と開発のベタープラクティス - Tech Blog - Recruit Engineer](https://engineer.recruit-lifestyle.co.jp/techblog/2018-03-16-go-ddd/)
- [Go で理解する DDD – ドメインサービス - taisablog](https://taisablog.com/archives/go-ddd-domain-service)
- [あなたの Go アプリ/ライブラリのパッケージ構成もっとシンプルでよくない？ | フューチャー技術ブログ](https://future-architect.github.io/articles/20200528/)
- [Go の package 構成と開発のベタープラクティス - Tech Blog - Recruit Engineer](https://engineer.recruit-lifestyle.co.jp/techblog/2018-03-16-go-ddd/)
