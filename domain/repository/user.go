package repository

import "simpleDDD/domain/model"

type UserRepository interface {
	SearchByName(name string) ([]*model.User, error)
}
