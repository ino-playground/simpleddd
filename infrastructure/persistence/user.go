package persistence

import (
	"simpleDDD/domain/model"
	"simpleDDD/domain/repository"

	"gorm.io/gorm"
)

type UserPersistence struct {
	Conn *gorm.DB
}

func NewUserPersistence(conn *gorm.DB) repository.UserRepository {
	return &UserPersistence{Conn: conn}
}

func (up *UserPersistence) SearchByName(name string) ([]*model.User, error) {
	var uma []*model.User

	if err := up.Conn.Take(&uma).Error; err != nil {
		return nil, err
	}

	db := up.Conn.Find(&uma)

	if name != "" {
		db = db.Where("name = ?", name).Find(&uma)
	}

	return uma, nil
}
