package handler

import (
	"encoding/json"
	"net/http"
	"simpleDDD/usecase"
)

type UserHandler interface {
	HandleUserGet(http.ResponseWriter, *http.Request)
}

type userHandler struct {
	userUseCase usecase.UserUseCase
}

func NewUserHandler(uu usecase.UserUseCase) UserHandler {
	return &userHandler{
		userUseCase: uu,
	}
}

func (uh userHandler) HandleUserGet(w http.ResponseWriter, r *http.Request) {
	name := r.FormValue("name")
	user, err := uh.userUseCase.SearchByName(name)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if err = json.NewEncoder(w).Encode(user); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}
