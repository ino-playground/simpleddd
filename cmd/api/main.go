package main

import (
	"fmt"
	"log"
	"net/http"
	"simpleDDD/config"
	"simpleDDD/infrastructure/persistence"
	"simpleDDD/interface/handler"
	"simpleDDD/usecase"
)

func main() {
	userPersistence := persistence.NewUserPersistence(config.Connect())
	userUseCase := usecase.NewUserUseCase(userPersistence)
	userHandler := handler.NewUserHandler(userUseCase)

	mux := http.NewServeMux()
	mux.Handle("/users", http.HandlerFunc(userHandler.HandleUserGet))

	fmt.Println("Server Running at http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", mux))
}
