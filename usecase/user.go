package usecase

import (
	"simpleDDD/domain/model"
	"simpleDDD/domain/repository"
)

type UserUseCase interface {
	SearchByName(name string) ([]*model.User, error)
}

type userUseCase struct {
	userRepository repository.UserRepository
}

func NewUserUseCase(ur repository.UserRepository) UserUseCase {
	return &userUseCase{
		userRepository: ur,
	}
}

func (uu userUseCase) SearchByName(name string) (user []*model.User, err error) {
	user, err = uu.userRepository.SearchByName(name)
	if err != nil {
		return nil, err
	}
	return user, err
}
